#!/usr/bin/env python
import numpy as np

gas_final = float(raw_input('enter pressure final mix[230]: ') or 230)
pp_gas_final = float(raw_input('enter o2 pp final mix[0.28]: ') or 0.28)
gas_existing = float(raw_input('enter pressure existing gas[80]: ') or 80)
pp_gas_existing = float(raw_input('enter o2 pp existing gas[0.25]: ') or 0.25)
pp_gas_1 = float(raw_input('enter o2 pp first top gas[0.21]: ') or 0.21)
pp_gas_2 = float(raw_input('enter o2 pp second top gas[0.32]: ') or 0.32)

gas_top = gas_final - gas_existing
tmp_gas_top = gas_final * pp_gas_final - gas_existing * pp_gas_existing

a = np.array([[pp_gas_1, pp_gas_2], [1, 1]])
b = np.array([tmp_gas_top, gas_top])
x = np.linalg.solve(a, b)
gas_1 = x[0]
gas_2 = x[1]

print('\nadd {0:.2f} bar of first top gas'.format(gas_1))
print('add {0:.2f} bar of second top gas to final '
      'pressure {1:.2f} bar'.format(gas_2, gas_final))
